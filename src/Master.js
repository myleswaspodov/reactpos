import React, { Component } from "react";
import { footer } from "./Layouts/Footer";
import Header from "./Layouts/Header";

export default class Master extends Component {
	render () {
		return (
			<div className="col-md-12">
				<Header />
				{ this.props.children }
				{ footer }
			</div>
		);
	}
}