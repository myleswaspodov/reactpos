import React, { Component } from 'react';

import { Table, Row, Col} from 'react-bootstrap';
import { connect } from 'react-redux';
import { listMaxx } from './actions';

class MaxxContainer extends Component {
	constructor() {
		super();

		// akses redux
		listMaxx();
	}

	render() {
		let menuAll = [];
		let tabelMenu = [];

		if (this.props.data.maxx.status === "success") {
			menuAll = this.props.data.maxx.result;

			console.log(menuAll);

			for (var i = 0; menuAll.length; i++) {
				console.log(i)
			}
		}

		return(
			<div>
				<Row className="show-grid">
				    <Col md={2} />

				    <Col md={8} >

						<Table responsive>
		    			  <thead>
		    			    <tr>
		    			      <th>#</th>
		    			      <th> Category </th>
		    			      <th> Menu </th>
		    			    </tr>
		    			  </thead>
		    			  <tbody>
		    			  	{
		    			  		
		    			  		tabelMenu
		    			  	}
		    			  </tbody>
		    			</Table>
				    </Col>
				    <Col md={2} />
				 </Row>
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	// console.log(state);
	return {data: state.maxx}
}

const Maxx = connect(mapStateToProps)(MaxxContainer);

export default Maxx;