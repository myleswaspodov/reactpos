import store from '../Reducers/store';

// SUKSES
function fetchPostsSuccess(payload) {
	store.dispatch({
		type: 'GET_MENU',
		maxx: payload
	});
}

// GAGAL
function fetchFailed() {
	store.dispatch({
		type: 'FAILED'
	});
}

// AKSES API
const listMaxx = () => {
	fetch("http://localhost/maxx-fase2-api/api/v1/products/list", {
		method: 'POST',
	  	headers: {
	    	'Content-Type': 'application/json',
	    	'Accept' : 'application/json',
	    	'Access-Control-Allow-Origin' : 'http://localhost/',
	    	'Access-Control-Allow-Credentials' : true
	  	},
	  	body: JSON.stringify({
	    	group_by_category: 1,
	  	})
	})
    .then(response => response.json())      
    .then(json => {
    	return fetchPostsSuccess(json);
    })
    .then(([response, json]) =>{
    	if(response.status === 200){
      		return fetchPostsSuccess(json);
      	}
      	else{
      		return fetchFailed()
      	}
    })
    .catch(error => {                 
        return fetchFailed();
    })
}

export { listMaxx };