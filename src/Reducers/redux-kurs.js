const initKurs = {
	kurs: []
}

const kursUpdate = (state = initKurs, action) => {
	switch (action.type) {
		case 'GET_KURS' :
			return Object.assign({}, state, {kurs: action.kurs});
		default:
			return state;
	}
}

export default kursUpdate;