const initMenu = {
	maxx: []
}

const maxxReducers = (state = initMenu, action) => {
	switch (action.type) {
		case 'GET_MENU' :
			return Object.assign({}, state, {maxx: action.maxx});
		default :
			return state;
	}
}

export default maxxReducers;