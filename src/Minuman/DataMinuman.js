const mimik = [{
  "id": 1,
  "nama_minuman": "Vodka",
  "harga_minuman": 10000,
}, {
  "id": 2,
  "nama_minuman": "Anggur Hijau",
  "harga_minuman": 10000,
}, {
  "id": 3,
  "nama_minuman": "Teh Tubruk",
  "harga_minuman": 10000,
}, {
  "id": 4,
  "nama_minuman": "Saparela",
  "harga_minuman": 10000,
}, {
  "id": 5,
  "nama_minuman": "Micin Hangat",
  "harga_minuman": 10000,
}, {
  "id": 6,
  "nama_minuman": "Kuah Bakso",
  "harga_minuman": 10000,
}, {
  "id": 7,
  "nama_minuman": "Ciyu",
  "harga_minuman": 10000,
}, {
  "id": 8,
  "nama_minuman": "Kopi Klotok",
  "harga_minuman": 10000,
}, {
  "id": 9,
  "nama_minuman": "Minuman Saset",
  "harga_minuman": 10000,
}, {
  "id": 10,
  "nama_minuman": "Oli",
  "harga_minuman": 10000,
}]

export default mimik;