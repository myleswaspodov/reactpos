import React, { Component } from 'react';
import { FormControl, Button, Table, Row, Col} from 'react-bootstrap';

// redux
import { connect } from 'react-redux';
import store from "../Reducers/store";
import {listMinuman} from './actions';

class MinumanRedux extends Component {
	constructor() {
		super();

		this.state = {
			filterDrinks: ''
		}
	}

	componentDidMount() {
		store.dispatch(listMinuman());
	}

	filterMinuman(event) {
		this.setState({
			filterDrinks: event.target.value
		});
	}

	render() {
		// kenapa ada drink karena index nya di action untuk get data adalah drink
		let minuman = this.props.drinks.drink;

		if (this.state.filterDrinks) {
			minuman = minuman.filter((minuman) => {
				let fullMinum = `${minuman.nama_minuman}`;
				return fullMinum.toLowerCase().includes(this.state.filterDrinks.toLowerCase());
			});
		}

		return (
			<div>
				<Row className="show-grid">
				    <Col md={2} />

				    <Col md={8} >

						<FormControl
				            type="text"
				            placeholder="Cari minuman"
				            onChange={this.filterMinuman.bind(this)}
				        />
		    			<Table responsive>
		    			  <thead>
		    			    <tr>
		    			      <th>#</th>
		    			      <th>Nama Minuman</th>
		    			      <th>Harga</th>
		    			      <th>Detail</th>
		    			    </tr>
		    			  </thead>
		    			  <tbody>

		    			  	{ minuman.map((edrinks, key) => 
		    	  		  		<tr key={ key }>
		    	  			      <td>{ key+1 }</td>
		    	  			      <td>{ edrinks.nama_minuman}</td>
		    	  			      <td>{ edrinks.harga_minuman}</td>
		    	  			      <td>  <Button bsStyle="primary" href={ "minuman/detail/" + edrinks.id }>Detail</Button> </td>
		    	  			    </tr>
		    			  	)}
		    			  </tbody>
		    			</Table>
				    </Col>
				    <Col md={2} />
				</Row>
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {drinks: state.namaMinuman}
}

const Minuman = connect(mapStateToProps)(MinumanRedux);

export default Minuman;