const food = [{
  "id": 1,
  "nama_makanan": "Pecel",
  "harga_makanan": 10000,
}, {
  "id": 2,
  "nama_makanan": "Soto",
  "harga_makanan": 10000,
}, {
  "id": 3,
  "nama_makanan": "Tahu Telor",
  "harga_makanan": 10000,
}, {
  "id": 4,
  "nama_makanan": "Tahu Gimbal",
  "harga_makanan": 10000,
}, {
  "id": 5,
  "nama_makanan": "Gudeg",
  "harga_makanan": 10000,
}, {
  "id": 6,
  "nama_makanan": "Bakmie Jawa",
  "harga_makanan": 10000,
}, {
  "id": 7,
  "nama_makanan": "Sengsu",
  "harga_makanan": 10000,
}, {
  "id": 8,
  "nama_makanan": "B2 Halal",
  "harga_makanan": 10000,
}, {
  "id": 9,
  "nama_makanan": "Unggas Goreng",
  "harga_makanan": 10000,
}, {
  "id": 10,
  "nama_makanan": "Sate Klatak",
  "harga_makanan": 10000,
}]

export default food;