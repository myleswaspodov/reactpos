import React, { Component } from 'react';
import { browserHistory } from 'react-router';
import { FormGroup, ControlLabel, FormControl, HelpBlock, Row, Col, Alert, Button } from 'react-bootstrap';
import food from "./DataFood";


export default class MakananDetail extends Component {
	componentWillMount() {
		// kasi variable harus ada kurung kurawal
		let {id} = this.props.params;
		
		// definisiin makananSemua
		this.cariMakanan = food.find((mkn) =>
			mkn.id === +id
		);
	}

	handleClick() {
		browserHistory.push('makanan');
	}

	render() {
		let makanan = this.cariMakanan
		if (makanan) {
			makanan = 
				<form>
		    	    <FieldGroup
		    	      id="id"
		    	      type="text"
		    	      label="Kode Makanan"
		    	      value={makanan.id}
		    	      readOnly
		    	    />
		    	    <FieldGroup
		    	      id="nama_makanan"
		    	      type="email"
		    	      label="Nama Makanan"
		    	      value={makanan.nama_makanan}
		    	      readOnly
		    	    />
		    	    <FieldGroup
		    	      id="harga_makanan"
		    	      type="text"
		    	      label="Harga Makanan"
		    	      help="Harga tahun 2018"
		    	      value={makanan.harga_makanan}
		    	      readOnly
		    	    />
		    	    <FormGroup>
	            		<Button bsStyle="success" onClick={ browserHistory.goBack }>Kembali</Button>
	            	</FormGroup>
		    	</form>
		}
		else {
			makanan = 
			<Alert bsStyle="danger">
	          <h4>Oh snap! Ngawurs Matamyu!</h4>
	          <p>
	            Change this and that and try again. Duis mollis, est non commodo
	            luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.
	            Cras mattis consectetur purus sit amet fermentum.
	          </p>
	          <p>
	            <Button bsStyle="danger" onClick={this.handleClick.bind(this)}>Mbalik'o cuk!</Button>
	          </p>
	        </Alert>
		}

		return (
			<div>
				<Row className="show-grid">
				    <Col md={2} />

				    <Col md={8} >
				    	{ makanan }
				    </Col>
				    
				    <Col md={2} />
				</Row>
				
			</div>
		);
	}
}

const FieldGroup = (({ id, label, help, ...props }) => {
	return (
    <FormGroup controlId={id}>
      <ControlLabel>{label}</ControlLabel>
      <FormControl {...props} />
      {help && <HelpBlock>{help}</HelpBlock>}
    </FormGroup>
  	);
})