import React, { Component } from "react";

import { Jumbotron, Button, PageHeader, Row, Col } from 'react-bootstrap';

export default class Home extends Component {
	constructor() {
		super();
		this.state = {
			waktuNow: "",
			salam: ""
		}
	}

	componentWillMount() {
		let waktuNow = new Date();

		var curHr = waktuNow.getHours();
		var salam = "";

		if (curHr < 12) {
			salam = "Good Morning";
		} else if (curHr < 18) {
			salam = "Good Afternoon";
		} else {
			salam = "Good Evening";
		}

		// set state
		this.setState({
			waktuNow: waktuNow,
			salam: salam
		});
	}

	render() {
		return (
			<div>
				<Row className="show-grid">
				    <Col md={1} />

				    <Col md={10} >
						<PageHeader>
							Welcome <small>at { this.state.waktuNow.toLocaleString() }</small>
						</PageHeader>
						<Jumbotron>
							<Row className="show-grid">
							    <Col md={1} />

							    <Col md={10} >
							    	<h1>{this.state.salam}, <small> Motherfucker! </small></h1>
							    	<p>
							    	    This is a simple hero unit, a simple jumbotron-style component for calling
							    	    extra attention to featured content or information.
							    	</p>
							    	<p>
							    	    <Button bsStyle="primary" href="https://www.facebook.com/dwiheruuu">Learn more</Button>
							    	</p>
							    </Col>
							    <Col md={1} />
							</Row>
						</Jumbotron>
				    </Col>
				      
				    <Col md={1} />
				 </Row>
			</div>
		);
	}
}